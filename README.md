# Principle

Write/Edit a human readible yaml file with comments and produce a FHIR example
resource in json format to serve as example.

The content of Example folder can be put on a web server to deliver:

- READ: <URL>/Patient/1
- SEARCH: POST <URL>/Patient/_search?whatever=something

Multiple files are generated from a 1.yaml source:

- 1.json : to be used for fhir validator
- 1 : to be use for READ
- _search : to be use for SEARCH


# Create a local FHIR endpoint

``` bash
# Simply clone this repository and use apache2 or nginx to access json
git clone <this-repository>
```

# Installation

``` bash
# for python
pip install -r bin/requirements.txt

# for lua
apt get install luarocks
luarocks install fhirformats

# for IG
apt get install jekyll
```


# Usage

``` bash
# generate the Patient examples
python bin/buildExample.py Patient

# validate the patient examples
java -jar lib/org.hl7.fhir.validator.jar  Example/Patient/1.json -ig Profile/geolocation.StructureDefinition.xml -ig Profile/Patient.StructureDefinition.xml -version 4.0.0 

# generate the implementation guide files
python bin/buildIg.py

# generate a FHIR xml from a json
lua bin/jsonToXml.lua path/to/json

# generate the implementation guide
(cd IG;make build)
```

# Implentation Guide Generation

The IG takes xml format as input. The `bin/jsonToXml.lua` script help to
translate. (see [fhir-formats](https://github.com/vadi2/fhir-formats) for
installation). There is automatic and manual aspect to generate the
Implentation Guide.

## Manual aspects

- Both `ig.json` and `resource/ig.xml` are maintained manually.
- Edit `IG/pages/_includes/<ResourceName>-intro.md`
- Edit `IG/pages/_includes/<ResourceName>-summary.md`

## Search

- the `implementedFields.yaml` file is the source of the `IG/pages/_includes/<ResourceName>-search.md`

## Profile

- add an `id` xml element
- the id `value` of the profile is used as `<ResourceName>`

## Example

- copy the 1.json into `IG/resources/`
- edit both `ig.json` and `resources/ig.xml`

## Extension

- copy the `Profile/structuredefinition-<extensionName>.xml` into `IG/resources/`
- edit both `ig.json` and `resources/ig.xml`

## CodeSystem

- can be created from Susana
- need to generate `codesystem-the-codesystem.xml`
- edit both `ig.json` and `resources/ig.xml`

## ValueSet

- can be created from Susana
- need to generate `valueset-the-valueset.xml`
- edit both `ig.json` and `resources/ig.xml`


# Profile Generation

# Profile/extension generation

Existing extension can be downloaded as xml format and integrated into firely.
For example the patient.address.geolocation extension can be downloaded from
[the fhir website](http://www.hl7.org/fhir/extension-geolocation.html) Since it
is a **address** extension, firely allow to add the extension to this element
only. The result of both **extension** and profiled **patient** can be saved
into xml format. The patient instance can then be added its geolocation as
described in the patient example.


# Resource Validation

documentation [here](http://wiki.hl7.org/index.php?title=Using_the_FHIR_Validator)


The validation tool validates informations defined into the profiles:
- cardinality (therefore presence/abscence of element)
- FHIR-PATH (a rule language [link1](https://www.hl7.org/fhir/profiling.html#discriminator) and  [link2](http://hl7.org/fhirpath/) )
- json or xml version
- terminologies aspects (value sets)

[maybe info there](https://chat.fhir.org/#narrow/stream/implementers/topic/FHIR.20validation)

- eg1: validating instanciated profiles:
	```bash
	$java -jar app/fhir_validator/org.hl7.fhir.validator.jar data/resource/MimicPatient.json -ig meta/extension/ -ig meta/profile/ -tx n/a
	  .. load FHIR from http://build.fhir.org/
	  .. connect to tx server @ null
	    (vnull-null)
	+  .. load IG from meta/extension/
	+  .. load IG from meta/profile/
	  .. validate
	==============!! Running without terminology server !!==============
	*FAILURE* validating data/resource/MimicPatient.json:  error:7 warn:1 info:0
	  Error @ Patient.communication.language : None of the codes provided are in the maximum value set http://hl7.org/fhir/ValueSet/all-languages (http://hl7.org/fhir/ValueSet/all-languages, and a code from this value set is required) (codes = http://hl7.org/fhir/CodeSystem/languages#en-US)
	  Error @ Patient (line 1, col2) : Profile http://fhir-icu/profile/MimicPatient, Element 'Patient.address: max allowed = 0, but found 1
	  Error @ Patient (line 1, col2) : Profile http://fhir-icu/profile/MimicPatient, Element 'Patient.communication: max allowed = 0, but found 1
	  Error @ Patient (line 1, col2) : Profile http://fhir-icu/profile/MimicPatient, Element 'Patient.identifier: max allowed = 0, but found 4
	  Error @ Patient (line 1, col2) : Profile http://fhir-icu/profile/MimicPatient, Element 'Patient.multipleBirth[x]: max allowed = 0, but found 1
	  Error @ Patient (line 1, col2) : Profile http://fhir-icu/profile/MimicPatient, Element 'Patient.telecom: max allowed = 0, but found 1
	  Error @ Patient (line 1, col2) : ExampleRuleMimic [Patient.birthDate > Patient.deceasedDateTime]
	  Warning @ Patient.communication.language.coding (line 117, col18) : Unknown Code System http://hl7.org/fhir/CodeSystem/languages
	```

- In this eg. the validator complains on cardinality. Interestingly, it processes the ExampleRuleMimic testing dates writen in the profile in FHIRPath rule language within forge tool.
- the tool does not scale = 30 sec to process a resource

- eg2: validating profiles:
	```bash
	$java -jar app/fhir_validator/org.hl7.fhir.validator.jar meta/profile/MimicPatient.structuredefition.xml -tx n/a
	  .. load FHIR from http://build.fhir.org/
	  .. connect to tx server @ null
	    (vnull-null)
	  .. validate
	*FAILURE* validating meta/profile/MimicPatient.structuredefinition.xml:  error:1 warn:0 info:0
	  Error @ StructureDefinition (line 2, col50) : All element definitions must have an id [snapshot.element.all(id.exists()) and differential.element.all(id.exists())]
	```
- in this eg. the validator complains all id should have an id. This testify forge does not perfect job. This should be correct in next release.


# ValueSet

## Fonctionalites attendues

- naviguer dans un value-set hierarchique ($expand)
  - demander le/les ancetres
  - demander le/les descendants
- demander des informations sur un concept donne ($lookup)


## expand

### aspects FHIR

### aspects OMOP

- utiliser la table calculee `concept_ancestor`
- coupler avec la table `concept`


## lookup

### aspects FHIR

### aspects OMOP

- utiliser la table `concept_relationship`
- coupler avec la table `concept`

import json
import yaml
import sys
import glob

filename = sys.argv[1]



# load yaml files
txtfiles = []
for file in glob.glob("Example/%s/*.yaml" % filename):
    txtfiles.append(file)

# write json files (for the READ), and build the bundle
bundleEntries = []
for fn in txtfiles:
    with open(fn) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    data["id"] = fn.replace(".yaml","").replace("Example/%s/" % filename,"")
    bundleEntries.append({"resource":data})
    jdata = json.dumps(data, indent=2, sort_keys=False)
    j = open(fn.replace(".yaml",""), "w")
    j.write(str(jdata))
    j.close()
    js = open(fn.replace(".yaml",".json"), "w")
    js.write(str(jdata))
    js.close()

# write the bundle (for the SEARCH)
with open("Example/Bundle.json") as f:
    bundle = json.load(f)
bundle["entry"] = bundleEntries
bundle["total"] = len(bundleEntries)
b = open("Example/%s/_search" % filename, "w")
b.write(str(json.dumps(bundle, indent=2, sort_keys=False)))
b.close()
